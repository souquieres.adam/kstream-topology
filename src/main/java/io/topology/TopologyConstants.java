package io.topology;

public class TopologyConstants {

    public static String SourceOperation = "Source";
    public static String SinkOperation = "Sink";
    public static String ProcessorOperation = "Processor";

}
