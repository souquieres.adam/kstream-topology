package io.topology.model;

import java.util.HashSet;
import java.util.Set;

public class TopologyModel {

    private int nbSubtopology = 0;
    private Set<String> inputTopicNames = new HashSet<>();
    private Set<String> outputTopicNames  = new HashSet<>();
    private Set<String> statestoreNames  = new HashSet<>();

    public Set<String> getOutputTopicNames() {
        return outputTopicNames;
    }

    public void addOutputTopicName(String newOutputTopic) {
        if(newOutputTopic.endsWith("-repartition")) return;

        this.outputTopicNames.add(newOutputTopic);
    }

    public Set<String> getStatestoreNames() {
        return statestoreNames;
    }

    public void addStatestoreName(String newStatestore) {
        this.statestoreNames.add(newStatestore);
    }

    public Set<String> getInputTopicNames() {
        return inputTopicNames;
    }

    public void addInputTopicName(String newInputTopicName) {

        if(newInputTopicName.endsWith("-repartition")) return;

        this.inputTopicNames.add(newInputTopicName);
    }

    public int getNbSubtopology() {
        return nbSubtopology;
    }

    public void setNbSubtopology(int nbSubtopology) {
        this.nbSubtopology = nbSubtopology;
    }
}
