package io.topology;

import io.topology.model.TopologyModel;

import java.util.Arrays;
import java.util.regex.Pattern;

import static io.topology.TopologyConstants.*;

public class TopologyParser {

    public static TopologyModel parse(String topologyString) {
        var topoLines = topologyString.split("\n");
        var nbSubTopo = 0;

        var patternSub = Pattern.compile("Sub-topology:");
        var patternOperation = Pattern.compile("(Source:|Sink:|Processor:)\\s\\S+\\s\\((topics:|topic:|stores:)\\s(.*)\\)");


        var matcherOperation = patternOperation.matcher(topologyString);
        var matcherSub = patternSub.matcher(topologyString);


        var newTopologyModel = new TopologyModel();

        while(matcherSub.find()) {
            nbSubTopo++;
        }

        newTopologyModel.setNbSubtopology(nbSubTopo);


        while(matcherOperation.find()) {

            var operationType = matcherOperation.group(1);
            var resourceNames = matcherOperation.group(3);

            if(operationType.startsWith(SourceOperation)) {
                newTopologyModel.addInputTopicName(resourceNames
                        .replaceAll("[\\[\\]]", "")
                );
                continue;
            }

            if(operationType.startsWith(SinkOperation)) {
                newTopologyModel.addOutputTopicName(resourceNames.replaceAll("[\\[\\]]", ""));
                continue;
            }

            var cleanedResourceNames = resourceNames.replaceAll("[\\[\\]]", "");

            if(cleanedResourceNames.isEmpty()) {
                continue;
            }

            var splittedResources = cleanedResourceNames.split(", ");

            if(splittedResources.length > 0) {
                Arrays.stream(splittedResources).forEach(newTopologyModel::addStatestoreName);
            } else {
                newTopologyModel.addStatestoreName(resourceNames);
            }
        }
        return newTopologyModel;
    }
}
