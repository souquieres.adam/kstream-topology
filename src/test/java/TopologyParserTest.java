import io.topology.TopologyParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TopologyParserTest {


    @Test
    public void shouldParse() {

        var result = TopologyParser.parse("Topologies:\n" +
                "   Sub-topology: 0\n" +
                "    Source: KSTREAM-SOURCE-0000000000 (topics: [InputTopic1])\n" +
                "      --> KSTREAM-FILTER-0000000001\n" +
                "    Processor: KSTREAM-FILTER-0000000001 (stores: [])\n" +
                "      --> KSTREAM-TRANSFORM-0000000002\n" +
                "      <-- KSTREAM-SOURCE-0000000000\n" +
                "    Source: KSTREAM-SOURCE-0000000013 (topics: [InputTopic2])\n" +
                "      --> KSTREAM-TRANSFORM-0000000014\n" +
                "    Processor: KSTREAM-TRANSFORM-0000000002 (stores: [Statestore1, Statestore2])\n" +
                "      --> KSTREAM-FILTER-0000000003\n" +
                "      <-- KSTREAM-FILTER-0000000001\n" +
                "    Processor: KSTREAM-TRANSFORM-0000000014 (stores: [Statestore1, Statestore2])\n" +
                "      --> KSTREAM-FILTER-0000000015\n" +
                "      <-- KSTREAM-SOURCE-0000000013\n" +
                "    Processor: KSTREAM-FILTER-0000000003 (stores: [])\n" +
                "      --> KSTREAM-FILTER-0000000004\n" +
                "      <-- KSTREAM-TRANSFORM-0000000002\n" +
                "    Processor: KSTREAM-FILTER-0000000015 (stores: [])\n" +
                "      --> KSTREAM-FILTER-0000000016\n" +
                "      <-- KSTREAM-TRANSFORM-0000000014\n" +
                "    Processor: KSTREAM-FILTER-0000000004 (stores: [])\n" +
                "      --> KSTREAM-BRANCH-0000000005\n" +
                "      <-- KSTREAM-FILTER-0000000003\n" +
                "    Processor: KSTREAM-FILTER-0000000016 (stores: [])\n" +
                "      --> KSTREAM-BRANCH-0000000017\n" +
                "      <-- KSTREAM-FILTER-0000000015\n" +
                "    Source: KSTREAM-SOURCE-0000000025 (topics: [InputTopic3])\n" +
                "      --> KSTREAM-TRANSFORM-0000000026\n" +
                "    Processor: KSTREAM-BRANCH-0000000005 (stores: [])\n" +
                "      --> KSTREAM-BRANCHCHILD-0000000006, KSTREAM-BRANCHCHILD-0000000007\n" +
                "      <-- KSTREAM-FILTER-0000000004\n" +
                "    Processor: KSTREAM-BRANCH-0000000017 (stores: [])\n" +
                "      --> KSTREAM-BRANCHCHILD-0000000018, KSTREAM-BRANCHCHILD-0000000019\n" +
                "      <-- KSTREAM-FILTER-0000000016\n" +
                "    Processor: KSTREAM-TRANSFORM-0000000026 (stores: [Statestore3])\n" +
                "      --> KSTREAM-FILTER-0000000027\n" +
                "      <-- KSTREAM-SOURCE-0000000025\n" +
                "    Processor: KSTREAM-FILTER-0000000027 (stores: [])\n" +
                "      --> KSTREAM-FILTER-0000000028\n" +
                "      <-- KSTREAM-TRANSFORM-0000000026\n" +
                "    Processor: KSTREAM-FILTER-0000000028 (stores: [])\n" +
                "      --> KSTREAM-BRANCH-0000000029\n" +
                "      <-- KSTREAM-FILTER-0000000027\n" +
                "    Processor: KSTREAM-BRANCH-0000000029 (stores: [])\n" +
                "      --> KSTREAM-BRANCHCHILD-0000000031, KSTREAM-BRANCHCHILD-0000000030\n" +
                "      <-- KSTREAM-FILTER-0000000028\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000006 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000009\n" +
                "      <-- KSTREAM-BRANCH-0000000005\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000007 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000008\n" +
                "      <-- KSTREAM-BRANCH-0000000005\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000018 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000021\n" +
                "      <-- KSTREAM-BRANCH-0000000017\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000019 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000020\n" +
                "      <-- KSTREAM-BRANCH-0000000017\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000031 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000032\n" +
                "      <-- KSTREAM-BRANCH-0000000029\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000008 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000010\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000007\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000009 (stores: [])\n" +
                "      --> KSTREAM-MERGE-0000000037\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000006\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000020 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000022\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000019\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000021 (stores: [])\n" +
                "      --> KSTREAM-MERGE-0000000037\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000018\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000032 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000034\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000031\n" +
                "    Processor: KSTREAM-MAP-0000000010 (stores: [])\n" +
                "      --> KSTREAM-TRANSFORMVALUES-0000000011\n" +
                "      <-- KSTREAM-MAPVALUES-0000000008\n" +
                "    Processor: KSTREAM-MAP-0000000022 (stores: [])\n" +
                "      --> KSTREAM-TRANSFORMVALUES-0000000023\n" +
                "      <-- KSTREAM-MAPVALUES-0000000020\n" +
                "    Processor: KSTREAM-MAP-0000000034 (stores: [])\n" +
                "      --> KSTREAM-TRANSFORMVALUES-0000000035\n" +
                "      <-- KSTREAM-MAPVALUES-0000000032\n" +
                "    Processor: KSTREAM-MERGE-0000000037 (stores: [])\n" +
                "      --> xxx-repartition-filter\n" +
                "      <-- KSTREAM-MAPVALUES-0000000009, KSTREAM-MAPVALUES-0000000021\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000030 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000033\n" +
                "      <-- KSTREAM-BRANCH-0000000029\n" +
                "    Processor: KSTREAM-TRANSFORMVALUES-0000000011 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000012\n" +
                "      <-- KSTREAM-MAP-0000000010\n" +
                "    Processor: KSTREAM-TRANSFORMVALUES-0000000023 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000024\n" +
                "      <-- KSTREAM-MAP-0000000022\n" +
                "    Processor: KSTREAM-TRANSFORMVALUES-0000000035 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000036\n" +
                "      <-- KSTREAM-MAP-0000000034\n" +
                "    Processor: xxx-repartition-filter (stores: [])\n" +
                "      --> xxxx-repartition-sink\n" +
                "      <-- KSTREAM-MERGE-0000000037\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000033 (stores: [])\n" +
                "      --> none\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000030\n" +
                "    Sink: KSTREAM-SINK-0000000012 (topic: DLQ_TOPIC)\n" +
                "      <-- KSTREAM-TRANSFORMVALUES-0000000011\n" +
                "    Sink: KSTREAM-SINK-0000000024 (topic: DLQ_TOPIC)\n" +
                "      <-- KSTREAM-TRANSFORMVALUES-0000000023\n" +
                "    Sink: KSTREAM-SINK-0000000036 (topic: DLQ_TOPIC)\n" +
                "      <-- KSTREAM-TRANSFORMVALUES-0000000035\n" +
                "    Sink: xxx-repartition-sink (topic: xxx-repartition)\n" +
                "      <-- xxx-repartition-filter\n" +
                "\n" +
                "  Sub-topology: 1\n" +
                "    Source: KSTREAM-SOURCE-0000000041 (topics: [InputTopic4])\n" +
                "      --> KSTREAM-TRANSFORM-0000000042\n" +
                "    Source: xxx-repartition-source (topics: [xxx-repartition])\n" +
                "      --> KSTREAM-TRANSFORM-0000000053\n" +
                "    Processor: KSTREAM-TRANSFORM-0000000042 (stores: [Statestore3])\n" +
                "      --> KSTREAM-FILTER-0000000043\n" +
                "      <-- KSTREAM-SOURCE-0000000041\n" +
                "    Processor: KSTREAM-TRANSFORM-0000000053 (stores: [Statestore3])\n" +
                "      --> KSTREAM-FILTER-0000000054\n" +
                "      <-- www-repartition-source\n" +
                "    Processor: KSTREAM-FILTER-0000000043 (stores: [])\n" +
                "      --> KSTREAM-FILTER-0000000044\n" +
                "      <-- KSTREAM-TRANSFORM-0000000042\n" +
                "    Processor: KSTREAM-FILTER-0000000054 (stores: [])\n" +
                "      --> KSTREAM-FILTER-0000000055\n" +
                "      <-- KSTREAM-TRANSFORM-0000000053\n" +
                "    Processor: KSTREAM-FILTER-0000000044 (stores: [])\n" +
                "      --> KSTREAM-BRANCH-0000000045\n" +
                "      <-- KSTREAM-FILTER-0000000043\n" +
                "    Processor: KSTREAM-FILTER-0000000055 (stores: [])\n" +
                "      --> KSTREAM-BRANCH-0000000056\n" +
                "      <-- KSTREAM-FILTER-0000000054\n" +
                "    Processor: KSTREAM-BRANCH-0000000045 (stores: [])\n" +
                "      --> KSTREAM-BRANCHCHILD-0000000046, KSTREAM-BRANCHCHILD-0000000047\n" +
                "      <-- KSTREAM-FILTER-0000000044\n" +
                "    Processor: KSTREAM-BRANCH-0000000056 (stores: [])\n" +
                "      --> KSTREAM-BRANCHCHILD-0000000057, KSTREAM-BRANCHCHILD-0000000058\n" +
                "      <-- KSTREAM-FILTER-0000000055\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000046 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000049\n" +
                "      <-- KSTREAM-BRANCH-0000000045\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000057 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000060\n" +
                "      <-- KSTREAM-BRANCH-0000000056\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000049 (stores: [])\n" +
                "      --> KSTREAM-MERGE-0000000064\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000046\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000060 (stores: [])\n" +
                "      --> KSTREAM-MERGE-0000000064\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000057\n" +
                "    Processor: KSTREAM-MERGE-0000000064 (stores: [])\n" +
                "      --> KSTREAM-BRANCH-0000000065\n" +
                "      <-- KSTREAM-MAPVALUES-0000000049, KSTREAM-MAPVALUES-0000000060\n" +
                "    Processor: KSTREAM-BRANCH-0000000065 (stores: [])\n" +
                "      --> KSTREAM-BRANCHCHILD-0000000066, KSTREAM-BRANCHCHILD-0000000067\n" +
                "      <-- KSTREAM-MERGE-0000000064\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000047 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000048\n" +
                "      <-- KSTREAM-BRANCH-0000000045\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000058 (stores: [])\n" +
                "      --> KSTREAM-MAPVALUES-0000000059\n" +
                "      <-- KSTREAM-BRANCH-0000000056\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000048 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000050\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000047\n" +
                "    Processor: KSTREAM-MAPVALUES-0000000059 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000061\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000058\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000066 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000068\n" +
                "      <-- KSTREAM-BRANCH-0000000065\n" +
                "    Processor: KSTREAM-BRANCHCHILD-0000000067 (stores: [])\n" +
                "      --> KSTREAM-MAP-0000000070\n" +
                "      <-- KSTREAM-BRANCH-0000000065\n" +
                "    Processor: KSTREAM-MAP-0000000050 (stores: [])\n" +
                "      --> KSTREAM-TRANSFORMVALUES-0000000051\n" +
                "      <-- KSTREAM-MAPVALUES-0000000048\n" +
                "    Processor: KSTREAM-MAP-0000000061 (stores: [])\n" +
                "      --> KSTREAM-TRANSFORMVALUES-0000000062\n" +
                "      <-- KSTREAM-MAPVALUES-0000000059\n" +
                "    Processor: KSTREAM-MAP-0000000068 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000069\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000066\n" +
                "    Processor: KSTREAM-MAP-0000000070 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000071\n" +
                "      <-- KSTREAM-BRANCHCHILD-0000000067\n" +
                "    Processor: KSTREAM-TRANSFORMVALUES-0000000051 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000052\n" +
                "      <-- KSTREAM-MAP-0000000050\n" +
                "    Processor: KSTREAM-TRANSFORMVALUES-0000000062 (stores: [])\n" +
                "      --> KSTREAM-SINK-0000000063\n" +
                "      <-- KSTREAM-MAP-0000000061\n" +
                "    Sink: KSTREAM-SINK-0000000052 (topic: DLQ_TOPIC)\n" +
                "      <-- KSTREAM-TRANSFORMVALUES-0000000051\n" +
                "    Sink: KSTREAM-SINK-0000000063 (topic: DLQ_TOPIC)\n" +
                "      <-- KSTREAM-TRANSFORMVALUES-0000000062\n" +
                "    Sink: KSTREAM-SINK-0000000069 (topic: OutputTopic1)\n" +
                "      <-- KSTREAM-MAP-0000000068\n" +
                "    Sink: KSTREAM-SINK-0000000071 (topic: OutputTopic2)\n" +
                "      <-- KSTREAM-MAP-0000000070");


        System.out.println("INPUT");
        result.getInputTopicNames().forEach(System.out::println);
        System.out.println("OUTPUT");
        result.getOutputTopicNames().forEach(System.out::println);
        System.out.println("STATESTORE");
        result.getStatestoreNames().forEach(System.out::println);

        assertEquals(4, result.getInputTopicNames().size());
        assertEquals(3, result.getOutputTopicNames().size() );
        assertEquals(3, result.getStatestoreNames().size());
        assertEquals(2, result.getNbSubtopology());
    }

}
