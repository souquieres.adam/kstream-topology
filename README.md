# kstream-topology

Helper to get some information about a Kafka Stream Topology

## Getting started

Simple helper taking a topopogy.describe() String output from Kafka Stream Topology Object and building an output Topology model with :

- Name of inputs topics (except -repartition)

- Name of outputs topics (except -repartition)

- Name of statestore

- Number of subtopologies in the topology

